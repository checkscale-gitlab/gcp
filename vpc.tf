variable "project_id" {
  description = "project id"
}

provider "google" {
  project = var.project_id
  region  = var.gcp_region
}

resource "google_compute_network" "vpc" {
  name                    = "${var.project_id}-vpc"
  auto_create_subnetworks = "false"
  description             = "vpc"
  routing_mode            = "REGIONAL"
  project                 = "${var.project_id}"
}

resource "google_compute_subnetwork" "subnet-pub" {
  name          = "${var.project_id}-subnet-pub"
  region        = var.gcp_region
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.0.0.0/12"

  log_config {
    aggregation_interval = "INTERVAL_10_MIN"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_subnetwork" "subnet-priv" {
  name          = "${var.project_id}-subnet-priv"
  region        = var.gcp_region
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.16.0.0/12"
  private_ip_google_access = true

  log_config {
    aggregation_interval = "INTERVAL_10_MIN"
    flow_sampling        = 0.5
    metadata             = "INCLUDE_ALL_METADATA"
  }
}