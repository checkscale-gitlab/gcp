#
# Reference: https://gitlab.com/gitlab-org/configure/examples/gke-with-terraform-and-agent-connection/-/blob/master/variables.tf
#
variable "gcp_project" {
  type = string
  description = "The name of the Google Cloud Project where the cluster is to be provisioned"
}

variable "gitlab_username" {
  type = string
  description = "The GitLab user providing the `gitlab_token` personal access token"
}

variable "gitlab_token" {
  type = string
  description = "Provide a GitLab access token with admin rights to the GitLab group set as the `gitlab_group` variable"
}

variable "gitlab_project_id" {
  type = string
  description = "The ID of this GitLab project."
}

variable "gcp_region" {
  type = string
  default = "southamerica-east1"
  description = "The name of the Google region where the cluster nodes are to be provisioned"
}

variable "cluster_name" {
  type = string
  default = "envdev"
  description = "The name of the cluster to appear on the Google Cloud Console"
}

variable "machine_type" {
  type = string
  default = "e2-micro"
  description = "The name of the machine type to use for the cluster nodes"
}

variable "cluster_description" {
  type = string
  default = "This cluster is defined in GitLab"
  description = "A description for the cluster. We recommend adding the $CI_PROJECT_URL variable to describe where the cluster is configured."
}

variable "token_name" {
  type    = string
  default = "kas-token"
  description = "The name used for the token at Agent registration"
}

variable "token_description" {
  type    = string
  default = "Token for KAS Agent Authentication"
  description = "The description used for the token at Agent registration"
}

variable "gitlab_graphql_api_url" {
  type    = string
  description = "The GraphQL API endpoint of the GitLab instance"
  default = "https://gitlab.com/api/graphql"
}
