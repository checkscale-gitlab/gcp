provider "gitlab" {
  base_url = "https://gitlab.com/api/v4/"
  token   = var.gitlab_token
}

terraform {
  backend "http" {
  }
}